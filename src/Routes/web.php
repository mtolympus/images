<?php

/**
 * Hermes Images API Routes
 * 
 * The package provides the following API endpoints which the vue components require for
 * their persistence.
 * 
 * @author      Nick Verheijen <verheijen.webdevelopment@gmail.com>
 * @version     1.0.0
 */

Route::group(["prefix" => "images"], function() {

    // Images
    Route::post("upload", "ImageApiController@postCreateImage")->name("api.images.create.post");
    Route::post("edit", "ImageApiController@postEditImage")->name("api.images.edit.post");
    Route::post("delete", "ImageApiController@postDeleteImage")->name("api.images.delete.post");

    // Tags
    Route::group(["prefix" => "tags"], function() {
        Route::post("create", "ImageTagApiController@postCreateTag")->name("api.images.tags.create.post");
        Route::post("edit", "ImageTagApiController@postEditTag")->name("api.images.tags.edit.post");
        Route::post("delete", "ImageTagApiController@postDeleteTag")->name("api.images.tags.delete.post");
    });

    // Categories
    Route::group(["prefix" => "categories"], function() {
        Route::post("create", "ImageCategoryApiController@postCreateCategory")->name("api.images.categories.create.post");
        Route::post("edit", "ImageCategoryApiController@postEditCategory")->name("api.images.categories.edit.post");
        Route::post("delete", "ImageCategoryApiController@postDeleteCategory")->name("api.images.categories.delete.post");
    });

});