<?php

use Hermes\Images\Models\Image;
use Hermes\Images\Models\ImageTag;
use Hermes\Images\Models\ImageCategory;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->emptyDatabase();

        $this->createTags();
        $this->createImages();
        $this->createCategories();
    }

    /**
     * Empty database
     * 
     * @return      void
     */
    public function emptyDatabase()
    {
        DB::table("images")->delete();
        DB::table("image_tags")->delete();
        DB::table("image_categories")->delete();
    }

    /**
     * Create tags
     * 
     * @return      void
     */
    public function createTags()
    {
        ImageTag::create(["name" => "bicycletta", "label" => "Bicycletta"]);
        ImageTag::create(["name" => "awesome", "label" => "Awesome"]);
        ImageTag::create(["name" => "great deal", "label" => "Great deal"]);
        ImageTag::create(["name" => "hello there miss", "label" => "Hello there miss"]);
        ImageTag::create(["name" => "love is for the birds", "label" => "Kutvogels"]);
    }
    
    /**
     * Create categories
     * 
     * @return      void
     */
    public function createCategories()
    {
        ImageCategory::create(["name" => "logos", "label" => "Logo's"]);
        ImageCategory::create(["name" => "products", "label" => "Products"]);
        ImageCategory::create(["name" => "bicycle products", "label" => "Bicycle products"]);
    }

    /**
     * Create images
     * 
     * @return      void
     */
    public function createImages()
    {
        
    }
}