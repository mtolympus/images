<?php

namespace Hermes\Images;

use Hermes\Images\Models\Image;
use Hermes\Images\Models\ImageTag;
use Hermes\Images\Models\ImageCategory;

class Images
{
    /**
     * Get all images
     * Returns a collection of images based on the optional criteria.
     * 
     * @param       ImageCategory                       The category the images should belong to                (optional)
     * @param       Array                               The tags the images should belong to (at least one)     (optional)
     * @return      Illuminate\Support\Collection
     */
    public function images(ImageCategory $category = null, array $tags = [])
    {
        // Array we're outputting
        $out = [];

        // Retrieve all of the images in the database
        $images = Image::all();

        // If there are images to process
        if ($images->count() == 0)
        {
            // Loop through all of the images
            foreach ($images as $image)
            {
                // Filter on category if needed; if this image does not belong to the specified category, skip it
                if (!is_null($category))
                {
                    if ($image->imageCategory->id != $category->id) continue;
                }

                // Filter on tags if needed
                if (count($tags) > 0)
                {
                    // Determine if the image has one of the tags
                    $has_tag = false;

                    // Loop through all of the tags we've received
                    foreach ($tags as $tag)
                    {
                        // If the tag is a string; we'll assume it's the tag's name
                        if (is_string($tag))
                        {
                            // Check if the image has the tag
                            if ($image->hasTagByName($tag))
                            {
                                // If so; flag it and break the loop
                                $has_tag = true;
                                break;
                            }
                        }
                        // If the tag is an instance of the ImageTag model
                        else if (is_a($tag, "Hermes\\Images\\Models\\ImageTag"))
                        {
                            // Check if the image has the tag
                            if ($image->hasTag($tag))
                            {
                                // If so; flag it and break the loop
                                $has_tag = true;
                                break;
                            }
                        }
                    }

                    // If the image does not have one of the tags, skip it
                    if (!$has_tag) continue;
                }

                // Convert the image's relative url to an absolute url
                $image->image_url = asset($image->image_url);

                // Add image to the output
                $out[] = $image;
            }
        }

        // Return the output array as a Collection
        return collect($out);
    }

    /**
     * Find image by id
     * 
     * @param       integer                             The ID of the image
     * @return      App\Models\Image or false
     */
    public function findById($id)
    {
        return Image::find($id);
    }

    /**
     * Categories
     * 
     * @return      Illuminate\Support\Collection
     */
    public function categories()
    {
        return ImageCategory::all();
    }

    /**
     * Tags
     * 
     * @return      Illuminate\Support\Collection
     */
    public function tags()
    {
        return ImageTag::all();
    }

    /**
     * Category dropdown data
     * 
     * @return      Illuminate\Support\Collection
     */
    public function categoryDropdownData()
    {
        $out = [];

        foreach (ImageCategory::all() as $imageCategory)
        {
            $out[] = [
                "text" => $imageCategory->label,
                "value" => $imageCategory->id
            ];
        }

        return collect($out);
    }

    /**
     * Tag dropdown data
     * 
     * @return      Illuminate\Support\Collection
     */
    public function tagDropdownData()
    {
        $out = [];

        foreach (ImageTag::all() as $imageTag)
        {
            $out[] = [
                "text" => $imageTag->label,
                "value" => $imageTag->id
            ];
        }

        return collect($out);
    }

    /**
     * Easy access to route endpoints
     * instead of having to remember and type really long route names
     * 
     * @return      string              The abslute URL to the requested endpoint
     */

    public function uploadImageEndpoint()
    {
        return route("api.images.create.post");
    }
    
    public function editImageEndpoint()
    {
        return route("api.images.edit.post");
    }
    
    public function deleteImageEndpoint()
    {
        return route("api.images.delete.post");
    }

    public function createTagEndpoint()
    {
        return route("api.images.tags.create.post");
    }

    public function editTagEndpoint()
    {
        return route("api.images.tags.edit.post");
    }

    public function deleteTagEndpoint()
    {
        return route("api.images.tags.delete.post");
    }

    public function createCategoryEndpoint()
    {
        return route("api.images.categories.create.post");
    }
    
    public function editCategoryEndpoint()
    {
        return route("api.images.categories.edit.post");
    }

    public function deleteCategoryEndpoint()
    {
        return route("api.images.categories.delete.post");
    }

    /**
     * Collection method
     */
    public function apiEndpoints()
    {
        return collect([
            "uploadImage" => $this->uploadImageEndpoint(),
            "editImage" => $this->editImageEndpoint(),
            "deleteImage" => $this->deleteImageEndpoint(),
            "createTag" => $this->createTagEndpoint(),
            "editTag" => $this->editTagEndpoint(),
            "deleteTag" => $this->deleteTagEndpoint(),
            "createCategory" => $this->createCategoryEndpoint(),
            "editCategory" => $this->editCategoryEndpoint(),
            "deleteCategory" => $this->deleteCategoryEndpoint()
        ]);
    }
}