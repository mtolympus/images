<?php

namespace Hermes\Images\Models;

use Illuminate\Database\Eloquent\Model;

class ImageCategory extends Model
{
    protected $table = "image_categories";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "name",
        "label"
    ];

    /**
     * Images
     * The images that have been assigned to this category
     */
    public function images()
    {
        return $this->hasMany("Hermes\Images\Models\Image");
    }
}