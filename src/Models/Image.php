<?php

namespace Hermes\Images\Models;

use Hermes\Images\Models\ImageTag;
use Hermes\Images\Models\ImageCategory;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "images";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "category_id",
        "title",
        "description",
        "url",
        "imageable_type",
        "imageable_id"
    ];

    /**
     * Scope a query to only include images of a given category
     * 
     * @param       Illuminate\Database\Eloquent\Builder                $query
     * @param       Hermes\Images\Models\ImageCategory                  $category
     * @return      Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCategory($query, ImageCategory $category)
    {
        return $query->where('category_id', $category->id);
    }
    
    /**
     * Category - an image belongs to an Image Category (optionally)
     */
    public function category()
    {
        return $this->belongsTo("Hermes\Images\Models\ImageCategory");
    }

    /**
     * Tags - an image can have many tags associated with it
     */
    public function tags()
    {
        return $this->morphToMany("Hermes\Images\Models\ImageTag", "image_taggables");
    }

    /**
     * Imageable - polymorphic relationship (one-to-many)
     */
    public function imageable()
    {
        return $this->morphTo();
    }
    
    /**
     * Has tag (by tag's name)
     * 
     * @param       string                                  Name of the tag we are checking for
     * @return      boolean
     */
    public function hasTagByName(string $tagName)
    {
        $tags = $this->tags;

        if ($tags->count() > 0)
        {
            foreach ($tags as $tag)
            {
                if ($tag->name == $tagName)
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Does this image have a tag
     * 
     * @param       ImageTag                                ImageTag we are checking for
     * @return      boolean
     */
    public function hasTag(ImageTag $tag)
    {
        $tags = $this->tags;

        if ($tags->count() > 0)
        {
            foreach ($tags as $t)
            {
                if ($t->id == $tag->id)
                {
                    return true;
                }
            }
        }

        return false;
    }
}