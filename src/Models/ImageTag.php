<?php

namespace Hermes\Images\Models;

use Illuminate\Database\Eloquent\Model;

class ImageTag extends Model
{
    protected $table = "image_tags";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "name",
        "label"
    ];

    /**
     * Images
     * The images that have been tagged with this tag
     */
    public function images()
    {
        return $this->morphedByMany("Hermes\Images\Models\Image", "image_taggables");
    }
}