<?php

namespace Hermes\Images\Providers;

use Hermes\Images\Images;
use Illuminate\Support\ServiceProvider;

class ImagesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootstrapConfig();
        $this->bootstrapDatabase();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register the routes of the application
        $this->app->register(RouteServiceProvider::class);

        // Register the Images service
        $this->app->singleton("images", function() {
            return new Images;
        });
    }

    /**
     * Bootstrap config(uration)
     * 
     * @return      void
     */
    private function bootstrapConfig()
    {
        // Setup config merging
        $this->mergeConfigFrom(__DIR__."/../Config/config.php", "hermes_images");

        // Setup config file publishing
        $this->publishes([__DIR__."/../Config/config.php" => config_path("hermes_images.php")]);
    }

    /**
     * Bootstrap database
     * migrations & seed
     * 
     * @return      void
     */
    private function bootstrapDatabase()
    {
        // Setup migration loading
        $this->loadMigrationsFrom(__DIR__."/../Database/migrations");

        // Setup migration & seed publishing (in case modifications to the database structure need to be made)
        $this->publishes([
            __DIR__."/../Database/migrations" => database_path("migrations"),
            __DIR__."/../Database/seeds" => database_path("seeds")
        ], "database");
    }
}