<?php

/**
 * Hermes Images Package
 * Configuration
 * 
 * @author          Nick Verheijen <verheijen.webdevelopment@gmail.com>
 * @version         1.0.0
 */

return [

    /**
     * Default upload destination
     * You can specify where to upload the images in the image-input-field component configuration
     * but if you choose not to pass the required parameter(s) the component will default to the 
     * destination specified below.
     * 
     * @param       string          the path relative to your app folder (app is root)
     */
    "default_upload_destination" => "images/uploads",

    

];