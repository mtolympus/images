<?php

namespace Hermes\Images\Http\Controllers;

use Hermes\Images\Models\ImageTag;
use Hermes\Images\Http\Requests\Tags\CreateImageTagRequest;
use Hermes\Images\Http\Requests\Tags\EditImageTagRequest;
use Hermes\Images\Http\Requests\Tags\DeleteImageTagRequest;

use App\Http\Controllers\Controller;

class ImageTagApiController extends Controller
{
    /**
     * Create image tag
     * 
     * @param           CreateImageTagRequest           $request
     * @return          JSON
     */
    public function postCreateImageTag(CreateImageTagRequest $request)
    {
        // Create the new tag
        $tag = ImageTag::create([
            "name" => $request->name,
            "label" => $request->label
        ]);

        // Return JSON response
        return response()->json([
            "success" => true,
            "tag" => $tag
        ]);
    }

    /**
     * Edit image tag
     * 
     * @param       EditImageTagRequest                 $request
     * @return      JSON
     */
    public function postEditImageTag(EditImageTagRequest $request)
    {
        // Grab the tag
        $tag = ImageTag::find($request->tag_id);

        // Update the tag
        $tag->name = $request->name;
        $tag->label = $request->label;
        $tag->save();

        // Return JSON response
        return response()->json([
            "status" => success,
            "tag" => $tag
        ]);
    }

    /**
     * Delete image tag
     * 
     * @param       DeleteImageTagRequest               $request
     * @return      JSON
     */
    public function postDeleteImageTag(DeleteImageTagRequest $request)
    {
        // Grab the tag
        $tag = ImageTag::find($request->tag_id);

        // Delete the tag
        $tag->delete();
        
        // Return JSON response
        return response()->json([
            "status" => success
        ]);
    }
}