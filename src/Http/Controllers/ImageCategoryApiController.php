<?php

namespace Hermes\Images\Http\Controllers;

use Hermes\Images\Models\ImageCategory;
use Hermes\Images\Http\Requests\Categories\EditImageCategory;
use Hermes\Images\Http\Requests\Categories\CreateImageCategory;
use Hermes\Images\Http\Requests\Categories\DeleteImageCategory;

use App\Http\Controllers\Controller;

class ImageCategoryApiController extends Controller
{
    /**
     * Create image category
     * 
     * @param           CreateImageRequest              $request
     * @return          JSON
     */
    public function postCreateCategory(CreateImageCategoryRequest $request)
    {
        // Create the new category
        $category = ImageCategory::create([
            "name" => $request->name,
            "label" => $request->label
        ]);

        // Return JSON response
        return response()->json([
            "status" => success,
            "category" => $category
        ]);
    }

    /**
     * Edit image category
     * 
     * @param           EditImageRequest              $request
     * @return          JSON
     */
    public function postEditCategory(EditImageCategoryRequest $request)
    {
        // Grab the category
        $category = ImageCategory::find($request->category_id);
        
        // Update the category
        $category->name = $request->name;
        $category->label = $request->label;
        $category->save();
        
        // Return JSON response
        return response()->json([
            "status" => success,
            "category" => $category
        ]);
    }

    /**
     * Delete image category
     * 
     * @param           DeleteImageRequest              $request
     * @return          JSON
     */
    public function postDeleteCategory(DeleteImageCategoryRequest $request)
    {
        // Grab the category
        $category = ImageCategory::find($request->category_id);

        // Delete the category
        $category->delete();

        // Return JSON response
        return response()->json([
            "status" => success,
            "category" => $category
        ]);
    }
}