<?php

namespace Hermes\Images\Http\Controllers;

use Uploader;

use Hermes\Images\Models\Image;
use Hermes\Imaegs\Models\ImageTag;
use Hermes\Images\Models\ImageCategory;
use Hermes\Images\Http\Requests\Images\EditImageRequest;
use Hermes\Images\Http\Requests\Images\CreateImageRequest;
use Hermes\Images\Http\Requests\Images\DeleteImageRequest;

use App\Http\Controllers\Controller;

class ImageApiController extends Controller
{
    /**
     * Create image
     * 
     * @param           CreateImageRequest              The request
     * @return          JSON
     */
    public function postCreateImage(CreateImageRequest $request)
    {
        // Determine the upload destination
        $destination = $request->has("upload_destination") and $request->upload_destination != "" ? $request->upload_destination : config("hermes_images.default_upload_destination");
        
        // Upload the image
        $image_url = Uploader::upload($request->file("image"), $destination);
        
        // Compose the data for the image (using required fields)
        $data = [
            "category_id" => $request->category_id,
            "title" => $request->title,
            "description" => $request->description,
            "url" => $image_url
        ];

        // Add optional data if we received it
        if ($request->has("parent_type")) $data["imageable_type"] = $request->parent_type;
        if ($request->has("parent_id")) $data["imageable_id"] = $request->parent_id;
        
        // Create the image
        $image = Image::create($data);

        // If we received some tags to associate with the image
        if ($request->has("tags") and $request->tags != "")
        {
            // Explode the composed string of ids into seperated ids
            $tag_ids = explode(",", $request->tags);

            // Extra failsafe to make sure we got data to process
            if (count($tag_ids) > 0)
            {
                // Sync the tags on the image we just created
                $image->tags()->sync($tag_ids);
            }
        }

        // Convert relative image url to an absolute image url
        $image->url = asset($image->url);

        // Pre-load the category and tags for the vue component
        $image->load("category");
        $image->load("tags");

        // Return response
        return response()->json([
            "success" => true,
            "image" => $image
        ]);
    }

    /**
     * Edit image
     * 
     * @param           EditImageRequest            The request
     * @return          JSON
     */
    public function postEditImage(EditImageRequest $request)
    {
        // Grab the image
        $image = Image::find($request->image_id);

        // Update the required fields (category is not required but we want to set it to null if we received null)
        $image->category_id = $request->category_id;
        $image->title = $request->title;
        $image->description = $request->description;

        // Update optional fields
        if ($request->has("parent_type")) $image->imageable_type = $request->parent_type;
        if ($request->has("parent_id")) $image->imageable_id = $request->parent_id;

        // Upload new image
        if ($request->hasFile("image"))
        {
            // Determine the upload destination
            if ($request->has("upload_destination") and $request->upload_destination)
            {
                $destination = $request->upload_destination;
            }
            else
            {
                $destination = config("hermes_images.default_upload_destination");
            }

            // Upload image
            $image_url = Uploader::upload($request->file("image"), $destination);
            
            // Update url
            $image->url = $image_url;
        }

        // Update tags
        if ($request->has("tags") and $request->tags != "")
        {
            // Explode the composed string of ids into seperated ids
            $tag_ids = explode(",", $request->tags);

            // Extra failsafe to make sure we got data to process
            if (count($tag_ids) > 0)
            {
                // Sync the tags on the image we just created
                $image->tags()->sync($tag_ids);
            }
        }

        // Save changes
        $image->save();

        // Convert relative image url to an absolute image url
        $image->url = asset($image->url);

        // Pre-load the category and tags for the vue component
        $image->load("category");
        $image->load("tags");

        // Return response
        return response()->json([
            "success" => true,
            "image" => $image
        ]);
    }

    /**
     * Delete image
     * 
     * @param           DeleteImageRequest          The request
     * @return          JSON
     */
    public function postDeleteImage(DeleteImageRequest $request)
    {
        // Grab the image
        $image = Image::find($request->image_id);

        // Delete the image
        $image->delete();

        // Return response
        return response()->json([
            "success" => true
        ]);
    }
}