<?php

namespace Hermes\Images\Http\Requests\Images;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class EditImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!Auth::check())
        {
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "upload_destination" => "",
            "image_id" => "required|exists:images,id",
            "category_id" => "",
            "title" => "",
            "description" => "",
            "tags" => "",
            "image" => "",
            "parent_type" => "",
            "parent_id" => "nullable|integer"
        ];
    }
}
