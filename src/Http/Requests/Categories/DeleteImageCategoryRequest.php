<?php

namespace Hermes\Images\Http\Requests\Categories;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class DeleteImageCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!Auth::check())
        {
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_id" => "required|exists:image_categories,id",
        ];
    }
}
