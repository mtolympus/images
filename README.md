# Laravel Images

This package provides your application with a generic image support. Add images to any model easily.

Comes with some goodies as well, more to be added

## Installation

### Step 1.
Require the package in your project by running the following command:

```
composer require hermes/images
```

### Step 2.

Create a new ImageModel and extend the package's ImageModel so you can define your inverse relationships (unfortunately required in a many-to-many polymorphic relationship which we need for re-using the same image instead of cloning it).

```
<?php

namespace App\Models;

use Hermes\Images\Models\Image;

class ImageModel extends Image
{
    public function products()
    {
        return $this->morphedByMany('App\Models\Product', 'imageables');
    }

    // etc..
}
```

### Step 2.

Publish the package's assets by running the following command:

```
php artisan vendor:publish --provider="Hermes\Images\Providers\ImagesServiceProvider"
```

Register the package's vue components:

```
todo
```

## Usage